# Change Log

All notable changes to the "intl-define-messages" extension will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Initial release

### Added
- Command Intl paste

## [1.0.1] - 2020-08-14
### Added
- Command Extract messages (Command + shift + i).
- Intl messages generate support idOnly property.
- Intl messages generate support multi line.

### Changed
- Rename command Intl paste to Intl messages generate.
- Change README file