## Generate Intl define messages from clipboard

Command name: Intl messages generate

- Copy the text.
- Command + i to generate messages.

Will generate Intl define messages from clipboard.

## Extract messages

Command name: Intl extract message

- Command + shift + i to run extract-intl.

Will extract Intl messages.