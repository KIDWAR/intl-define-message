import * as vscode from 'vscode';
import { camelize, genMessage } from './format';
export function activate(context: vscode.ExtensionContext) {
	console.log('Congratulations, your extension "intl-define-messages" is now active!');

	let intlMessagesGenerate = vscode.commands.registerCommand('intl-define-messages.intlMessagesGenerate', async () => {
		const clipboardContent = await vscode.env.clipboard.readText();
		const arr = clipboardContent.split("\n");
		const activeEditor = vscode.window.activeTextEditor;
		for (let i = 0; i < arr.length; i++) {
			const element = arr[i];
			await paste(element, activeEditor);
		}
	});

	let intlExtract = vscode.commands.registerCommand('intl-define-messages.intlExtract', () => {
		vscode.window.showInformationMessage(`Extract message...`);
		const terminal = vscode.window.createTerminal("Extract-intl");
		terminal.show();
		terminal.sendText("npm run extract-intl & exit");
	});

	context.subscriptions.push(intlMessagesGenerate);
	context.subscriptions.push(intlExtract);

	const paste = async (data = "", activeEditor = vscode.window.activeTextEditor) => {
		if (data) {
			const id = camelize(data);
			const messageObj = genMessage(id, data);
			if (activeEditor) {
				let selection = activeEditor.selection;
				await activeEditor.edit(editBuilder => {
					editBuilder.insert(selection.start, messageObj);
				});
			}
		} else {
			vscode.window.showInformationMessage(`Intl Paste please copy message first.`);
		}
	};
}
export function deactivate() { }
