export const camelize = (str = "") => {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
    return index === 0 ? word.toLowerCase() : word.toUpperCase();
  }).replace(/\s+/g, '').replace(/[^a-zA-Z0-9 ]/g, "").normalize("NFD").replace(/[\u0300-\u036f]/g, "");
};

export const genMessage = (id = "", message = "") => {
  return `
  ${id}: {
    id: \`\$\{scope\}.${id}\`,
    defaultMessage: '${message}',
    idOnly: '${id}'
  },`;
};